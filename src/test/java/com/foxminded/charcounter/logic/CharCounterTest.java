package com.foxminded.charcounter.logic;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.LinkedHashMap;
import java.util.Map;

class CharCounterTest {

    private CharCounter charCounter;
    private Map<Character, Integer> charMap;

    @BeforeEach
    void createCharCounterObjects() {
        charCounter = new CharCounter();
        charMap = new LinkedHashMap<>();
    }

    @Test
    void getCharSequence_shouldReturnEmptyString_whenInputIsNull() {
        charMap.put(' ', 0);
        Assertions.assertEquals(charMap, charCounter.getCharSequence(null));
    }

    @Test
    void getCharSequence_shouldReturnEmptyString_whenInputIsEmptyString() {
        charMap.put(' ', 0);
        Assertions.assertEquals(charMap, charCounter.getCharSequence(""));
    }

    @Test
    void getCharSequence_shouldReturnOneCharacter_whenInputStringHasOneCharacter() {
        charMap.put('b', 1);
        Assertions.assertEquals(charMap, charCounter.getCharSequence("b"));
    }

    @Test
    void getCharSequence_shouldReturnOneCharacter_whenInputIsStringWithSameCharacters() {
        charMap.put('c', 3);
        Assertions.assertEquals(charMap, charCounter.getCharSequence("ccc"));
    }

    @Test
    void getCharSequence_shouldReturnCountOfDifferentCharacters_whenInputStringHasDifferentCharacters() {
        charMap.put('a', 1);
        charMap.put('b', 1);
        charMap.put('c', 1);
        Assertions.assertEquals(charMap, charCounter.getCharSequence("abc"));
    }

    @Test
    void getCharSequence_shouldReturnSequenceOfCharacters_whenInputStringHasDifferentCountOfCharacters() {
        charMap.put('a', 1);
        charMap.put('b', 2);
        charMap.put('c', 3);
        charMap.put(' ', 1);
        Assertions.assertEquals(charMap, charCounter.getCharSequence("abc cbc"));
    }

    @Test
    void getCharSequence_shouldReturnSecondCountingFasterThanFirst_whenInputStringIsSame() {
        String inputString = "This test should return first time greater than second";
        long firstCountTime = System.nanoTime();
        charCounter.getCharSequence(inputString);
        firstCountTime = System.nanoTime() - firstCountTime;
        long secondCountTime = System.nanoTime();
        charCounter.getCharSequence(inputString);
        secondCountTime = System.nanoTime() - secondCountTime;
        Assertions.assertTrue(firstCountTime > secondCountTime);
    }
}
