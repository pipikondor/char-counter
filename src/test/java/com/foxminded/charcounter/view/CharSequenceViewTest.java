package com.foxminded.charcounter.view;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.LinkedHashMap;
import java.util.Map;

class CharSequenceViewTest {

    private CharSequenceView charSequenceView;
    private StringBuilder exceptedView;
    private Map<Character, Integer> charMap;

    @BeforeEach
    void createShowSequenceObjects() {
        charSequenceView = new CharSequenceView();
        exceptedView = new StringBuilder();
        charMap = new LinkedHashMap<>();
    }

    @Test
    void showSequence_shouldReturnViewOfZeroSpace_whenInputIsEmptyWord() {
        charMap.put(' ', 0);
        exceptedView.append("\" \" - 0");
        Assertions.assertEquals(exceptedView.toString(), charSequenceView.showSequence(charMap));
    }

    @Test
    void showSequence_shouldReturnViewOfOneSpace_whenInputIsOneSpace() {
        charMap.put(' ', 1);
        exceptedView.append("\" \" - 1");
        Assertions.assertEquals(exceptedView.toString(), charSequenceView.showSequence(charMap));
    }

    @Test
    void showSequence_shouldReturnViewOfWordAndCountOfChars_whenInputStringStartWithLetter() {
        charMap.put('a', 1);
        charMap.put('c', 2);
        charMap.put(' ', 1);
        exceptedView.append("\"a\" - 1").append(System.lineSeparator());
        exceptedView.append("\"c\" - 2").append(System.lineSeparator());
        exceptedView.append("\" \" - 1");
        Assertions.assertEquals(exceptedView.toString(), charSequenceView.showSequence(charMap));
    }

    @Test
    void showSequence_shouldReturnViewOfWordAndCountOfChars_whenInputStringStartWithSpace() {
        charMap.put(' ', 1);
        charMap.put('a', 1);
        charMap.put('c', 2);
        exceptedView.append("\" \" - 1").append(System.lineSeparator());
        exceptedView.append("\"a\" - 1").append(System.lineSeparator());
        exceptedView.append("\"c\" - 2");
        Assertions.assertEquals(exceptedView.toString(), charSequenceView.showSequence(charMap));
    }
}
