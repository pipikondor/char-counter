package com.foxminded.charcounter.logic;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.IntStream;

public class CharCounter {

    private Map<String, Map<Character, Integer>> wordsCache;

    public CharCounter() {
        this.wordsCache = new HashMap<>();
    }

    public Map<Character, Integer> getCharSequence(String word) {
        if (word == null) {
            word = "";
        }
        if (wordsCache.containsKey(word)) {
            return wordsCache.get(word);
        }
        Map<Character, Integer> charSequence = countCharacters(word);
        wordsCache.put(word, charSequence);
        return charSequence;
    }

    private Map<Character, Integer> countCharacters(String word) {
        Map<Character, Integer> wordAsMap = new LinkedHashMap<>();
        if (word.length() == 0) {
            wordAsMap.put(' ', 0);
            return wordAsMap;
        }
        IntStream wordAsStream = word.chars();
        wordAsStream.forEach(letter -> wordAsMap.merge((char)letter, 1, Integer::sum));
        return wordAsMap;
    }
}
