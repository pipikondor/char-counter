package com.foxminded.charcounter;

import com.foxminded.charcounter.logic.CharCounter;
import com.foxminded.charcounter.view.CharSequenceView;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        CharCounter charCounter = new CharCounter();
        CharSequenceView charSequenceView = new CharSequenceView();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your sequence: ");
        while (scanner.hasNextLine()) {
            String sequence = scanner.nextLine();
            System.out.println(charSequenceView.showSequence(charCounter.getCharSequence(sequence)));
            System.out.println("Enter your sequence: ");
        }
    }
}
