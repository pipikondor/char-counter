package com.foxminded.charcounter.view;

import java.util.Map;

public class CharSequenceView {

    public String showSequence(Map<Character, Integer> charSequence) {
        StringBuilder sequenceView = new StringBuilder();
        for (Map.Entry<Character, Integer> countOfChar : charSequence.entrySet()) {
            sequenceView.append("\"");
            sequenceView.append(countOfChar.getKey());
            sequenceView.append("\"");
            sequenceView.append(" - ");
            sequenceView.append(countOfChar.getValue());
            sequenceView.append(System.lineSeparator());
        }
        return sequenceView.substring(0, sequenceView.lastIndexOf(System.lineSeparator()));
    }
}
